export interface Heroe{
    id?:string;
    superhero:string;
    publisher: publisher;
    alter_ego:string;
    first_appearance:string;
    characters:string;
    alt_img?:string;
}

export enum publisher{
    DCComics = "Dc Comics",
    MarvelComics = "Marvel Comics",
}

